package controls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import procedureETL.TuplaChiave;
import procedureETL.FormatoTuplaIniziale;

public class ControlloDuplicati {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private ArrayList<FormatoTuplaIniziale> nochecked;
	private Map<TuplaChiave, FormatoTuplaIniziale> traced;
	private ArrayList<FormatoTuplaIniziale> checked;
	
	public ControlloDuplicati(ArrayList<FormatoTuplaIniziale> nochecked){
		this.nochecked = new ArrayList<FormatoTuplaIniziale>(nochecked);
		traced = new HashMap<TuplaChiave, FormatoTuplaIniziale>();
	}
	
	public void generaMapping(){
		traced.put(generaChiave(nochecked.get(0)), nochecked.get(0));
		
		for(int i=1; i<nochecked.size(); i++){
			if(traced.containsKey(generaChiave(nochecked.get(i)))){
				LOGGER.warning("Errore: La tupla " + i + " e' duplicata.");
			}
			else traced.put(generaChiave(nochecked.get(i)), nochecked.get(i));
		}
		checked = new ArrayList<FormatoTuplaIniziale>(traced.values());
	}
	
	public TuplaChiave generaChiave(FormatoTuplaIniziale tuplaIniziale){
		TuplaChiave tuplaChiave=new TuplaChiave(tuplaIniziale.getID(), tuplaIniziale.getIDQuartiere(), tuplaIniziale.getIDUso(), tuplaIniziale.getConsumoQuartiere(), tuplaIniziale.getConsumoCO2(), tuplaIniziale.getData());
		return tuplaChiave;
	}
	
	public ArrayList<FormatoTuplaIniziale> getNonDuplicati(){
		return checked;
	}
}