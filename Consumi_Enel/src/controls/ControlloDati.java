package controls;


import java.util.ArrayList;

import procedureETL.FormatoTuplaFinale;
import procedureETL.FormatoTuplaIniziale;

public class ControlloDati {
	private ArrayList<FormatoTuplaIniziale> recordIniziali;
	private ArrayList<FormatoTuplaFinale> recordFinali = new ArrayList<FormatoTuplaFinale>();
    
	public ControlloDati(ArrayList<FormatoTuplaIniziale> recordIniziali){
		this.recordIniziali = new ArrayList<FormatoTuplaIniziale>(recordIniziali);		
	}
	
	public void datiFinali() {
		for(int i=0;i<recordIniziali.size();i++) {
			FormatoTuplaFinale newTuple = new FormatoTuplaFinale();
			newTuple.setID(recordIniziali.get(i).getID());
			newTuple.setIDQuartiere(recordIniziali.get(i).getIDQuartiere());
			newTuple.setIDMunicipio(recordIniziali.get(i).getIDMunicipio());
			newTuple.setIDUso(recordIniziali.get(i).getIDUso());
			newTuple.setAnnoMese(recordIniziali.get(i).getAnnoMese());
			newTuple.setConsumoQuartiere(recordIniziali.get(i).getConsumoQuartiere());
			newTuple.setUtenzeQuartiere(recordIniziali.get(i).getUtenzeQuartiere());
			newTuple.setConsumoCO2(recordIniziali.get(i).getConsumoCO2());
			newTuple.setConsumoMedioUtenza(recordIniziali.get(i).getConsumoMedioUtenza());
			newTuple.setQuartiere(recordIniziali.get(i).getQuartiere());
			newTuple.setPopolazioneQuartiere(recordIniziali.get(i).getPopolazioneQuartiere());
			newTuple.setAnno(recordIniziali.get(i).getAnno());
			newTuple.setNumeroMese(recordIniziali.get(i).getNumeroMese());
			newTuple.setMese(recordIniziali.get(i).getMese());
			newTuple.setData(recordIniziali.get(i).getData());
			newTuple.setMeseAnno(recordIniziali.get(i).getMeseAnno());
			newTuple.setConsumoSuPopolazione(recordIniziali.get(i).getConsumoSuPopolazione());
			newTuple.setUso(recordIniziali.get(i).getUso());
			newTuple.setLatitudine(recordIniziali.get(i).getLatitudine());
			newTuple.setLongitudine(recordIniziali.get(i).getLongitudine());
			newTuple.setStagione(recordIniziali.get(i).getData().calcoloStagione());
			
			recordFinali.add(newTuple);
		}
	}
	public ArrayList<FormatoTuplaFinale> getTupleTrasformate(){
		return recordFinali;
	}
}
