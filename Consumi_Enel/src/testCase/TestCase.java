package testCase;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import procedureETL.ControlloCampo;
import exception.LunghezzaIntestazioneException;
import exception.OrdineIntestazioneException;

class TestCase {
	
	String NonInOrdine = "hits_Totale;hits_hits__Indice;hits_hits__Type;hits_hits__ID;"
			+ "hits_hits__IDQuartiere;hits_hits__IDMunicipio;hits_hits__IDUso;hits_hits__AnnoMese;"
			+ "hits_hits__ConsumoQuartiere;hits_hits__UtenzeQuartiere;hits_hits__ConsumoCO2;hits_hits__ConsumoMedioUtenza;"
			+ "hits_hits__Quartiere;hits_hits__PopolazioneQuartiere;hits_hits__Anno;hits_hits__NumeroMese;hits_hits__Mese;"
			+ "hits_hits__Data;hits_hits__MeseAnno;hits_hits__ConsumoSuPopolazione;hits_hits__Uso;hits_hits__Latitudine;"
			+ "hits_hits__Longitudine;timed_out";
	
	String CampiSbagliati = "timed_out;hits_Totale;hits_hits__Indice;hits_hits__Type;hits_hits__ID;"
			+ "hits_hitiere;hits_hits__IDcipio;hits_hits__IDUso;hits_hits__AnnoMese;"
			+ "hits__ConsumoQuartiere;hits_hits__UtenzeQuartiere;hits_hits__ConsumoCO2;hits_hits__CoMedioUtenza;"
			+ "hits_hits__Quartiere;hits_hits__PopolazioneQuartiere;hits__Anno;hits_hits__NumeroMese;hits_hits__Mese;"
			+ "hits_hits__Data;hits_hits__MeseAnno;hs__ConsumoSuPopolazione;hitsso;hits_hitudine;"
			+ "hits_hits__Line";
	
	String SenzaCampo = "timed_out;hits_Totale;hits_hits__Type;hits_hits__ID;"
			+ "hits_hits__IDQuartiere;hits_hits__IDMunicipio;hits_hits__IDUso;hits_hits__AnnoMese;"
			+ "hits_hits__ConsumoQuartiere;hits_hits__UtenzeQuartiere;hits_hits__ConsumoCO2;hits_hits__ConsumoMedioUtenza;"
			+ "hits_hits__Quartiere;hits_hits__PopolazioneQuartiere;hits_hits__Anno;hits_hits__NumeroMese;hits_hits__Mese;"
			+ "hits_hits__Data;hits_hits__MeseAnno;hits_hits__ConsumoSuPopolazione;hits_hits__Uso;hits_hits__Latitudine;"
			+ "hits_hits__Longitudine";
	
	String ConPiuUnCampo = "timed_out;hits_Totale;hits_hits__Indice;hits_hits__Type;hits_hits__ID;"
			+ "hits_hits__IDQuartiere;hits_hits__IDMunicipio;hits_hits__IDUso;hits_hits__AnnoMese;"
			+ "hits_hits__ConsumoQuartiere;hits_hits__UtenzeQuartiere;hits_hits__ConsumoCO2;hits_hits__ConsumoMedioUtenza;"
			+ "hits_hits__Quartiere;hits_hits__PopolazioneQuartiere;hits_hits__Anno;hits_hits__NumeroMese;hits_hits__Mese;"
			+ "hits_hits__Data;hits_hits__MeseAnno;hits_hits__ConsumoSuPopolazione;hits_hits__Uso;hits_hits__Latitudine;"
			+ "hits_hits__Longitudine;Stagione";

	@Test
	void ControlloIntestazione() {
		Assertions.assertThrows(OrdineIntestazioneException.class, () -> {
			ControlloCampo.checkIntestazione(NonInOrdine);
		});
		
		Assertions.assertThrows(OrdineIntestazioneException.class, () -> {
			ControlloCampo.checkIntestazione(CampiSbagliati);
		});
		
		Assertions.assertThrows(LunghezzaIntestazioneException.class, () -> {
			ControlloCampo.checkIntestazione(SenzaCampo);
		});
		
		Assertions.assertThrows(LunghezzaIntestazioneException.class, () -> {
			ControlloCampo.checkIntestazione(ConPiuUnCampo);
		});	
	}
	@Test
	void testTimedOut() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkTimedOut(null, 1));
		});

		assertFalse(ControlloCampo.checkTimedOut("dsndis", 0));
		assertTrue(ControlloCampo.checkTimedOut("false", 1));
		assertFalse(ControlloCampo.checkTimedOut("35", 2));
		assertTrue(ControlloCampo.checkTimedOut("true", 1));
	}
	@Test
	void testTotale() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkTotale(null, 2));
		});

		assertFalse(ControlloCampo.checkTotale("prova", 0));
		assertTrue(ControlloCampo.checkTotale("1267", 1));
		assertTrue(ControlloCampo.checkTotale("20", 2));
	}
	
	@Test
	void testIndice() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkIndice(null,0));
		});

		assertTrue(ControlloCampo.checkIndice("vw_enel_energia_quart", 0));
		assertFalse(ControlloCampo.checkIndice("28", 1));
		assertFalse(ControlloCampo.checkIndice("test", 2));
	}
	@Test
	void testType() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkType(null,0));
		});

		assertTrue(ControlloCampo.checkType("default", 23));
		assertFalse(ControlloCampo.checkType("3232", 24));
		assertFalse(ControlloCampo.checkType("false", 25));
	}
	
	
	
	@Test
	void testID() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkID(null,0));
		});

		assertFalse(ControlloCampo.checkID("", 581));
		assertTrue(ControlloCampo.checkID("162017052", 582));
		assertFalse(ControlloCampo.checkID("testo", 583));
	}
	
	@Test
	void testIDQuartiere() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkIDQuartiere(null,0));
		});

		assertFalse(ControlloCampo.checkIDQuartiere("", 581));
		assertTrue(ControlloCampo.checkIDQuartiere("3", 582));
		assertFalse(ControlloCampo.checkIDQuartiere("testo", 583));
	}
	@Test
	void testIDMunicipio() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkIDMunicipio(null,0));
		});

		assertFalse(ControlloCampo.checkIDMunicipio("", 4));
		assertTrue(ControlloCampo.checkIDMunicipio("2", 5));
		assertFalse(ControlloCampo.checkIDMunicipio("testo", 7));
		assertFalse(ControlloCampo.checkIDMunicipio("80", 40));
	}
	@Test
	void testIDUso() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkIDMunicipio(null,0));
		});

		assertFalse(ControlloCampo.checkIDMunicipio("", 4));
		assertTrue(ControlloCampo.checkIDMunicipio("1", 5));
		assertFalse(ControlloCampo.checkIDMunicipio("prova", 90));
		assertFalse(ControlloCampo.checkIDMunicipio("80", 780));
	}
	
	
	@Test
	void testAnnoMese() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkAnnoMese(null,0));
		});

		assertFalse(ControlloCampo.checkAnnoMese("", 1));
		assertTrue(ControlloCampo.checkAnnoMese("201802", 5));
		assertFalse(ControlloCampo.checkAnnoMese("testo", 754));
		assertFalse(ControlloCampo.checkAnnoMese("8043", 440));
	}
	@Test
	void testConsumoQuartiere() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkConsumoQuartiere(null,0));
		});

		assertFalse(ControlloCampo.checkConsumoQuartiere("", 3));
		assertTrue(ControlloCampo.checkConsumoQuartiere("12300000", 5));
		assertFalse(ControlloCampo.checkConsumoQuartiere("testo", 725));
		assertTrue(ControlloCampo.checkConsumoQuartiere("8043", 30));
	}
	
	@Test
	void testUtenzeQuartiere() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkUtenzeQuartiere(null,0));
		});

		assertFalse(ControlloCampo.checkUtenzeQuartiere("", 54));
		assertTrue(ControlloCampo.checkUtenzeQuartiere("26", 55));
		assertFalse(ControlloCampo.checkUtenzeQuartiere("tdso", 725));
		assertTrue(ControlloCampo.checkUtenzeQuartiere("8043", 56));
	}
	@Test
	void testConsumoCO2() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkCO2(null,0));
		});

		assertFalse(ControlloCampo.checkCO2("", 54));
		assertTrue(ControlloCampo.checkCO2("4580000000", 55));
		assertFalse(ControlloCampo.checkCO2("tdso", 725));
	}
	@Test
	void testConsumoMedioUtenza() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkConsumoMedio(null,0));
		});

		assertFalse(ControlloCampo.checkConsumoMedio("", 1));
		assertTrue(ControlloCampo.checkConsumoMedio("117002", 2));
		assertFalse(ControlloCampo.checkConsumoMedio("prova", 3));
	}
	
	@Test
	void testQuartiere() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkQuartiere(null,0));
		});

		assertFalse(ControlloCampo.checkQuartiere("", 1));
		assertTrue(ControlloCampo.checkQuartiere("VIII - PICONE", 2));
		assertFalse(ControlloCampo.checkQuartiere("nulla", 3));
		assertFalse(ControlloCampo.checkQuartiere("8", 3));
	}
	@Test
	void testPopolazione() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkPopolazioneQuartiere(null,0));
		});

		assertFalse(ControlloCampo.checkPopolazioneQuartiere("", 73));
		assertTrue(ControlloCampo.checkPopolazioneQuartiere("101553", 3));
		assertFalse(ControlloCampo.checkPopolazioneQuartiere("nulla", 374));
	}
	
	@Test
	void ControlloLongitudine() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkLongitudine(null, 0));
		});
		assertFalse(ControlloCampo.checkLongitudine("42,09876", 1));
		assertTrue(ControlloCampo.checkLongitudine("41,09435685", 2));
		assertFalse(ControlloCampo.checkLongitudine("40,09876", 3));
		assertTrue(ControlloCampo.checkLongitudine("41", 4));
	}
	
	@Test
	void ControlloLatitudine() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkLatitudine(null, 0));
		});
		assertFalse(ControlloCampo.checkLatitudine("16", 1));
		assertTrue(ControlloCampo.checkLatitudine("17", 2));
		assertFalse(ControlloCampo.checkLatitudine("17,59876", 3));
		assertTrue(ControlloCampo.checkLatitudine("16,563728", 4));
	}
	
	@Test
	void ControlloUso() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkUso(null, 0));
		});
		assertFalse(ControlloCampo.checkUso("Industriale", 1));
		assertTrue(ControlloCampo.checkUso("Altro", 2));
		assertFalse(ControlloCampo.checkUso("Domestica", 3));
		assertTrue(ControlloCampo.checkUso("Residenziale", 4));
	}
	
	@Test
	void ControlloConsumoSuPopolazione() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkConsumoSuPopolazione(null, 0));
		});
		assertFalse(ControlloCampo.checkConsumoSuPopolazione("-1324", 1));
		assertTrue(ControlloCampo.checkConsumoSuPopolazione("20,23", 2));
		assertFalse(ControlloCampo.checkConsumoSuPopolazione("-1,23", 3));
		assertTrue(ControlloCampo.checkConsumoSuPopolazione("1", 4));
	}
	
	@Test
	void ControlloMeseAnno() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkMeseAnno(null, 0));
		});
		assertFalse(ControlloCampo.checkMeseAnno("Ago-18", 1));
		assertTrue(ControlloCampo.checkMeseAnno("ago-17", 2));
		assertFalse(ControlloCampo.checkMeseAnno("18-set", 3));
		assertTrue(ControlloCampo.checkMeseAnno("set-15", 4));
		assertTrue(ControlloCampo.checkMeseAnno("gen-11", 5));
		assertTrue(ControlloCampo.checkMeseAnno("mag-16", 6));
	}
	
	@Test
	void ControlloData() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkData(null, 0));
		});
		assertFalse(ControlloCampo.checkData("", 42));
		assertFalse(ControlloCampo.checkData("22/4/1090", 43));
		assertTrue(ControlloCampo.checkData("12/12/2019", 44));
		assertFalse(ControlloCampo.checkData("34/12/2014", 45));
		assertFalse(ControlloCampo.checkData("querty", 46));
		assertTrue(ControlloCampo.checkData("14/12/2017", 47));
		assertFalse(ControlloCampo.checkData("22|12\2019", 47));
	}
	
	@Test
	void ControlloMese() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkMese(null, 0));
		});
		assertFalse(ControlloCampo.checkMese("agosto", 1));
		assertTrue(ControlloCampo.checkMese("Agosto", 2));
		assertFalse(ControlloCampo.checkMese("set", 3));
		assertTrue(ControlloCampo.checkMese("Gennaio", 4));
		assertFalse(ControlloCampo.checkMese("12", 3));
	}
	
	@Test
	void ControlloNumeroMese() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkNumeroMese(null, 0));
		});
		assertFalse(ControlloCampo.checkNumeroMese("0", 1));
		assertTrue(ControlloCampo.checkNumeroMese("5", 2));
		assertFalse(ControlloCampo.checkNumeroMese("15", 3));
		assertTrue(ControlloCampo.checkNumeroMese("8", 4));
		assertFalse(ControlloCampo.checkNumeroMese("controllo", 3));
	}
	
	@Test
	void ControlloAnno() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			assertFalse(ControlloCampo.checkAnno(null, 0));
		});
		assertFalse(ControlloCampo.checkAnno("1980", 1));
		assertFalse(ControlloCampo.checkAnno("millenovecento", 2));
		assertTrue(ControlloCampo.checkAnno("2000", 3));
		assertTrue(ControlloCampo.checkAnno("2030", 4));
		assertFalse(ControlloCampo.checkAnno("19", 3));
	}

}
