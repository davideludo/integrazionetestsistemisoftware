package exception;

import logger.MyLogger;

public class OrdineIntestazioneException extends Throwable {
	private static final long serialVersionUID = 1L;
	
	public OrdineIntestazioneException() {
		System.out.println("ERRORE: Consultare il file di log per informazioni.");
		MyLogger.severe.add("Errore: Valore campi intestazione non consentiti o in ordine errato.");
	}
}