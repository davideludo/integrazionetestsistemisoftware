package exception;

import logger.MyLogger;

public class LunghezzaIntestazioneException extends Throwable {
	private static final long serialVersionUID = 1L;

	public LunghezzaIntestazioneException() {
		System.out.println("ERRORE: Consultare il file di log per informazioni");
		MyLogger.severe.add("Errore: Intestazione file non consentita.");
	}
}