package procedureETL;

public class TuplaChiave {
	private int id;
	private int idQuartiere;
	private int idUso;
	private long consumoQuartiere;
	private long CO2;
	private Data data;
	public TuplaChiave() {
		
	}
	public TuplaChiave(int id, int idQuartiere, int idUso, long consumoQuartiere, long cO2, Data data) {
		super();
		this.id = id;
		this.idQuartiere = idQuartiere;
		this.idUso = idUso;
		this.consumoQuartiere = consumoQuartiere;
		this.CO2 = cO2;
		this.data = data;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdQuartiere() {
		return idQuartiere;
	}
	public void setIdQuartiere(int idQuartiere) {
		this.idQuartiere = idQuartiere;
	}
	public int getIdUso() {
		return idUso;
	}
	public void setIdUso(int idUso) {
		this.idUso = idUso;
	}
	public float getConsumoQuartiere() {
		return consumoQuartiere;
	}
	public void setConsumoQuartiere(long consumoQuartiere) {
		this.consumoQuartiere = consumoQuartiere;
	}
	public long getCO2() {
		return CO2;
	}
	public void setCO2(long cO2) {
		CO2 = cO2;
	}
	public Data getData() {
		return data;
	}
	public void setData(Data data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "TuplaChiave [id=" + id + ", idQuartiere=" + idQuartiere + ", idUso=" + idUso + ", consumoQuartiere="
				+ consumoQuartiere + ", CO2=" + CO2 + ", data=" + data + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (CO2 ^ (CO2 >>> 32));
		result = prime * result + Float.floatToIntBits(consumoQuartiere);
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + id;
		result = prime * result + idQuartiere;
		result = prime * result + idUso;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TuplaChiave other = (TuplaChiave) obj;
		if (CO2 != other.CO2)
			return false;
		if (Float.floatToIntBits(consumoQuartiere) != Float.floatToIntBits(other.consumoQuartiere))
			return false;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (id != other.id)
			return false;
		if (idQuartiere != other.idQuartiere)
			return false;
		if (idUso != other.idUso)
			return false;
		return true;
	}
}