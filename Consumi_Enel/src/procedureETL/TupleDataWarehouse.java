package procedureETL;

import java.util.ArrayList;
import java.util.logging.Logger;

import procedureETL.FormatoTuplaFinale;

public class TupleDataWarehouse {
	private ArrayList<FormatoTuplaFinale> dwRecords;
	private ArrayList<FormatoTuplaFinale> dwCandidates;
	private ArrayList<FormatoTuplaFinale> dwNewRecords = new ArrayList<FormatoTuplaFinale>();
	private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public TupleDataWarehouse(ArrayList<FormatoTuplaFinale> dwRecords, ArrayList<FormatoTuplaFinale> dwCandidates) {
		this.dwRecords = new ArrayList<FormatoTuplaFinale>(dwRecords);
		this.dwCandidates = new ArrayList<FormatoTuplaFinale>(dwCandidates);
	}
	
	public void generateDWNewRecords() {
		int cont = 0;
		for (int i = 0; i < dwCandidates.size(); i++) {
			if (!dwRecords.contains(dwCandidates.get(i))) {
				dwNewRecords.add(dwCandidates.get(i));
			} else {
				cont++;
				LOGGER.warning("La tupla con ID" + dwCandidates.get(i).getID()
						+ " e' gia' presente nel file finale.");
			}
		}
		LOGGER.info("Tuple duplicate trovate: " + cont);
	}

	public ArrayList<FormatoTuplaFinale> getDWNewRecords() {
		return dwNewRecords;
	}
}