package procedureETL;

import java.util.logging.Logger;

import enumerations.Mese;
import enumerations.MeseAbbreviato;
import exception.LunghezzaIntestazioneException;
import exception.OrdineIntestazioneException;

public class ControlloCampo {
	private final static String[] formatoIntestazione = { "timed_out", "hits_Totale", "hits_hits__Indice", "hits_hits__Type",
			"hits_hits__ID", "hits_hits__IDQuartiere", "hits_hits__IDMunicipio", "hits_hits__IDUso", "hits_hits__AnnoMese", "hits_hits__ConsumoQuartiere", "hits_hits__UtenzeQuartiere",
			"hits_hits__ConsumoCO2", "hits_hits__ConsumoMedioUtenza", "hits_hits__Quartiere", "hits_hits__PopolazioneQuartiere", "hits_hits__Anno", "hits_hits__NumeroMese",
			"hits_hits__Mese", "hits_hits__Data", "hits_hits__MeseAnno", "hits_hits__ConsumoSuPopolazione", "hits_hits__Uso", "hits_hits__Latitudine", "hits_hits__Longitudine" };
	
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private static String csvSplit = ";";
	private static String[] campiIntestazione = new String[21];
	
	public static void checkIntestazione(String intestazioneCsv) throws LunghezzaIntestazioneException, OrdineIntestazioneException {
		
		campiIntestazione = intestazioneCsv.split(csvSplit);

		// CONTROLLO SE IL NUMERO DI CAMPI DEL FILE E' CORRETTO
		if (campiIntestazione.length != formatoIntestazione.length) throw new LunghezzaIntestazioneException(); 
		
			System.out.println(campiIntestazione[0]);
		// CONTROLLO DEL NOME DEI CAMPI FACENTI PARTE DELL'INTESTAZIONE
		for(int i = 0; i<campiIntestazione.length; i++){
			if(campiIntestazione[i].compareTo(formatoIntestazione[i]) != 0) {
				throw new OrdineIntestazioneException();
			}
		}
	}
	
	//INIZIO CONTROLLI SU OGNI CAMPO IN BASE AL TIPO
	public static boolean checkTimedOut(String timed_out, int cont){
		
		boolean risultato = true;
		
		if(timed_out.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di Timed_out nullo. Tupla " + cont + "!");
		}else if(!(timed_out.equals("true")||timed_out.equals("false"))) {
			risultato = false;
			LOGGER.warning("Errore: Valore di Time_out non permesso. Tupla " + cont + "!");
		}
		
		return risultato;	
	}

	public static boolean checkTotale(String totale, int cont){
		int int_totale;
		boolean risultato = true;
		
		if(totale.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di Totale nullo. Tupla " + cont + "!");
		}
			try{
				int_totale = Integer.parseInt(totale);
			}catch(NumberFormatException e){
				risultato = false;
				LOGGER.warning("Errore: Valore di Totale non permesso. Tupla " + cont + "!");
			}
		
		return risultato;	
	}

	public static boolean checkIndice(String indice, int cont){
		boolean risultato = true;
		
		if(indice.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di Indice nullo. Tupla " + cont + "!");
		
		}else if(!indice.equals("vw_enel_energia_quart")) {
			risultato = false;
			LOGGER.warning("Errore: Valore di Indice non permesso. Tupla " + cont + "!");
		}
		
		return risultato;	
	}

	public static boolean checkType(String type, int cont){
		boolean risultato = true;
		
		if(type.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di Type nullo. Tupla " + cont + "!");
		
		}else if(!type.equals("default")) {
		risultato = false;
		LOGGER.warning("Errore: Valore di Type non permesso. Tupla " + cont + "!");
		}
		
		return risultato;	
	}

	public static boolean checkID(String id, int cont){
		int int_id;
		boolean risultato = true;
		
		if(id.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di ID nullo. Tupla " + cont + "!");
	}
		try{
			int_id = Integer.parseInt(id);
		}catch(NumberFormatException e){
			risultato = false;
			LOGGER.warning("Errore: Formato di ID non permesso. Tupla " + cont + "!");
		}
		return risultato;	
	}

	public static boolean checkIDQuartiere(String idQuartiere, int cont){
		int int_idQuartiere;
		boolean risultato = true;
		
		if(idQuartiere.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di IDQuartiere nullo. Tupla " + cont + "!");
	}
		try{
			int_idQuartiere = Integer.parseInt(idQuartiere);
			if(int_idQuartiere<1||int_idQuartiere>17) {
				risultato = false;
				LOGGER.warning("Errore: Valore di IDQuartiere non compreso tra i valori consentiti. Tupla " + cont + "!");
			}
		}catch(NumberFormatException e){
			risultato = false;
			LOGGER.warning("Errore: Formato di IDQuartiere non permesso. Tupla " + cont + "!");
		}
		return risultato;	
	}

	public static boolean checkIDMunicipio(String idMunicipio, int cont){
		int int_idMunicipio;
		boolean risultato = true;
		
		if(idMunicipio.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di IDMunicipio nullo. Tupla " + cont + "!");
	}
		try{
			int_idMunicipio = Integer.parseInt(idMunicipio);
			if(int_idMunicipio<1||int_idMunicipio>5) {
				risultato = false;
				LOGGER.warning("Errore: Valore di IDMunicipio non compreso tra i valori consentiti. Tupla " + cont + "!");
			}
		}catch(NumberFormatException e){
			risultato = false;
			LOGGER.warning("Errore: Formato di IDMunicipio non permesso. Tupla " + cont + "!");
		}
		return risultato;	
	}

	public static boolean checkIDUso(String idUso, int cont){
		int int_idUso;
		boolean risultato = true;
		
		if(idUso.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di IDUso nullo. Tupla " + cont + "!");
	}
		try{
			int_idUso = Integer.parseInt(idUso);
			if(int_idUso<1||int_idUso>2) {
				risultato = false;
				LOGGER.warning("Errore: Valore di IDUso non compreso tra i valori consentiti. Tupla " + cont + "!");
			}
		}catch(NumberFormatException e){
			risultato = false;
			LOGGER.warning("Errore: Formato di IDUso non permesso. Tupla " + cont + "!");
		}
		return risultato;	
	}

	public static boolean checkAnnoMese(String annoMese, int cont){
		boolean risultato = true;
		
		if(annoMese.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di AnnoMese nullo. Tupla " + cont + "!");
	}	
		if(annoMese.length()==6) {
			 String stAnno=annoMese.substring(0,4);
			 String stMese=annoMese.substring(4);
			 try{
					if(Integer.parseInt(stAnno)<1990||Integer.parseInt(stMese)<1||Integer.parseInt(stMese)>12) {
						risultato = false;
						LOGGER.warning("Errore: Valore di AnnoMese non compreso tra i valori consentiti. Tupla " + cont + "!");
					}
				}catch(NumberFormatException e){
					risultato = false;
					LOGGER.warning("Errore: Formato di AnnoMese non permesso. Tupla " + cont + "!");
				}
				 
		}else {
			risultato = false;
			LOGGER.warning("Errore: Numero di cifre di AnnoMese non consentito. Tupla " + cont + "!");
		}
		return risultato;	
	}

	public static boolean checkConsumoQuartiere(String consumoQuartiere, int cont){
		long l_consumoQuartiere;
		boolean risultato = true;
		
		if(consumoQuartiere.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di ConsumoQuartiere nullo. Tupla " + cont + "!");
	}
		try{
			l_consumoQuartiere = Long.parseLong(consumoQuartiere);
			if(l_consumoQuartiere<0) {
				risultato = false;
				LOGGER.warning("Errore: Valore di ConsumoQuartiere negativo. Tupla " + cont + "!");
			}
		}catch(NumberFormatException e){
			risultato = false;
			LOGGER.warning("Errore: Formato di ConsumoQuartiere non permesso. Tupla " + cont + "!");
		}
		return risultato;	
	}
	
	public static boolean checkUtenzeQuartiere(String utenzeQuartiere, int cont){
		int int_utenzeQuartiere;
		boolean risultato = true;
		
		if(utenzeQuartiere.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di UtenzeQuartiere nullo. Tupla " + cont + "!");
	}
		try{
			int_utenzeQuartiere = Integer.parseInt(utenzeQuartiere);
			if(int_utenzeQuartiere<0) {
				risultato = false;
				LOGGER.warning("Errore: Valore di UtenzeQuartiere negativo. Tupla " + cont + "!");
			}
		}catch(NumberFormatException e){
			risultato = false;
			LOGGER.warning("Errore: Formato di UtenzeQuartiere non permesso. Tupla " + cont + "!");
		}
		return risultato;
	}	

	public static boolean checkCO2(String CO2, int cont){
		long long_CO2;
		boolean risultato = true;
		
		if(CO2.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di ConsumoCO2 nullo. Tupla " + cont + "!");
	}
		try{
			long_CO2 = Long.parseLong(CO2);
			if(long_CO2<0) {
				risultato = false;
				LOGGER.warning("Errore: Valore di ConsumoCO2 negativo. Tupla " + cont + "!");
			}
		}catch(NumberFormatException e){
			risultato = false;
			LOGGER.warning("Errore: Formato di ConsumoCO2 non permesso. Tupla " + cont + "!");
		}
		return risultato;	
	}	

	public static boolean checkConsumoMedio(String consumoMedio, int cont){
		int int_consumoMedio;
		boolean risultato = true;
		
		if(consumoMedio.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di ConsumoMedio nullo. Tupla " + cont + "!");
	}
		try{
			int_consumoMedio = Integer.parseInt(consumoMedio);
			if(int_consumoMedio<0) {
				risultato = false;
				LOGGER.warning("Errore: Valore di ConsumoMedio negativo. Tupla " + cont + "!");
			}
		}catch(NumberFormatException e){
			risultato = false;
			LOGGER.warning("Errore: Formato di ConsumoMedio non permesso. Tupla " + cont + "!");
		}
		return risultato;	
	}

	public static boolean checkQuartiere(String Quartiere, int cont){
		boolean risultato = true;
		if(Quartiere.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di Quartiere nullo. Tupla " + cont + "!");
		}else {
			if(!(Quartiere.equals("I - SAN NICOLA")||Quartiere.equals("II - MURAT")||Quartiere.equals("III - MADONNELLA")||Quartiere.equals("IV - LIBERTA'")
					||Quartiere.equals("V - JAPIGIA")||Quartiere.equals("VI - SAN PASQUALE")||Quartiere.equals("VII - CARRASSI")||Quartiere.equals("VIII - PICONE")
					||Quartiere.equals("IX - STANIC")||Quartiere.equals("X  - MARCONI - SAN GIROLAMO - FESCA")||Quartiere.equals("XI - SAN PAOLO")||Quartiere.equals("XII - PALESE - MACCHIE")
					||Quartiere.equals("XIII - SANTO SPIRITO")||Quartiere.equals("XIV - CARBONARA DI BARI")||Quartiere.equals("XV - CEGLIE DEL CAMPO")
					||Quartiere.equals("XVI - LOSETO")||Quartiere.equals("XVII - TORRE A MARE"))) {
				risultato = false;
				LOGGER.warning("Errore: Valore di Quartiere non permesso. Tupla " + cont + "!");
			}
		}
		return risultato;	
	}

	public static boolean checkPopolazioneQuartiere(String popolazioneQuartiere, int cont){
		int int_popolazioneQuartiere;
		boolean risultato = true;
		
		if(popolazioneQuartiere.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di PopolazioneQuartiere nullo. Tupla " + cont + "!");
	}
		try{
			int_popolazioneQuartiere = Integer.parseInt(popolazioneQuartiere);
			if(int_popolazioneQuartiere<0) {
				risultato = false;
				LOGGER.warning("Errore: Valore di PopolazioneQuartiere negativo. Tupla " + cont + "!");
			}
		}catch(NumberFormatException e){
			risultato = false;
			LOGGER.warning("Errore: Formato di PopolazioneQuartiere non permesso. Tupla " + cont + "!");
		}
		return risultato;	
	}

	public static boolean checkAnno(String anno, int cont){
		int int_anno;
		boolean risultato = true;
		
		if(anno.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di Anno nullo. Tupla " + cont + "!");
	}
		try{
			int_anno = Integer.parseInt(anno);
			if(int_anno<1990) {
				risultato = false;
				LOGGER.warning("Errore: Valore di Anno non consentito. Tupla " + cont + "!");
			}
		}catch(NumberFormatException e){
			risultato = false;
			LOGGER.warning("Errore: Formato di Anno non permesso. Tupla " + cont + "!");
		}
		return risultato;	
	}


	public static boolean checkNumeroMese(String numeroMese, int cont){
		int int_numeroMese;
		boolean risultato = true;
		
		if(numeroMese.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di NumeroMese nullo. Tupla " + cont + "!");
	}
		try{
			int_numeroMese = Integer.parseInt(numeroMese);
			if(int_numeroMese<1||int_numeroMese>12) {
				risultato = false;
				LOGGER.warning("Errore: Valore di NumeroMese non compreso tra i valori consentiti. Tupla " + cont + "!");
			}
		}catch(NumberFormatException e){
			risultato = false;
			LOGGER.warning("Errore: Formato di NumeroMese non permesso. Tupla " + cont + "!");
		}
		return risultato;	
	}

	public static boolean checkMese(String mese, int cont){
		boolean risultato=false;
		if(mese.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di Mese nullo. Tupla " + cont + "!");
		}else {
			for(Mese m:Mese.values()) {
				if(m.name().equals(mese)){
					risultato=true;
				}
			}
		}
		return risultato;	
	}

	public static boolean checkData(String data, int cont){
		String[] d = data.split("/");
		boolean risultato = true;
		
		if((d.length == 0) || (d == null)){
			risultato = false;
			LOGGER.warning("Errore: Valore di Data nullo. Tupla " + cont + "!");
		}else{
			
			try {
				int gg = Integer.parseInt(d[0]);
				int mm = Integer.parseInt(d[1]);
				int aaaa = Integer.parseInt(d[2]);
				
				if(gg<=0 || gg>31) {
					risultato = false;
					LOGGER.warning("Errore: Valore del GIORNO in Data non permesso. Tupla " + cont + "!");
				}
				
				if(mm<=0 || mm>12){
					risultato = false;
					LOGGER.warning("Errore: Valore del MESE in Data non permesso. Tupla " + cont + "!");
				}
				
				if(aaaa<1970) {
					risultato = false;
					LOGGER.warning("Errore: Valore dell'ANNO in Data non permesso. Tupla " + cont + "!");
				}
				
			}catch(NumberFormatException e){
				risultato = false;
				LOGGER.warning("Errore: Formato di Data non permesso. Tupla " + cont + "!");
			}
		}
		return risultato;
	}
	
	public static boolean checkMeseAnno(String meseAnno, int cont){
		boolean risultato = true;
		
		if(meseAnno.length()!=6){
			risultato = false;
			LOGGER.warning("Errore: Valore di MeseAnno nullo. Tupla " + cont + "!");
		}else {
			String[] mA = meseAnno.split("-");
			boolean meseValido=false;
			for(MeseAbbreviato mAbb:MeseAbbreviato.values()) {
				if(mAbb.name().equals(mA[0])){
					meseValido=true;
				}
			}
			if(!meseValido) {
				risultato = false;
				LOGGER.warning("Errore: Valore del MESE in MeseAnno non permesso. Tupla " + cont + "!");
			}
		
			try{
				int anno = Integer.parseInt(mA[1]);
			}catch(NumberFormatException e){
				risultato = false;
				LOGGER.warning("Errore: Valore dell'ANNO in MeseAnno non permesso. Tupla " + cont + "!");
			}
		}
		return risultato;	
	}

	public static boolean checkConsumoSuPopolazione(String consumoSuPop, int cont){
		float fl_consumoSuPop;
		boolean risultato = true;
		
		if(consumoSuPop.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di ConsumoSuPopolazione nullo. Tupla " + cont + "!");
		}
		try{
			String cons=consumoSuPop.replace(",",".");
			fl_consumoSuPop = Float.parseFloat(cons);
			if(fl_consumoSuPop<0) {
				risultato = false;
				LOGGER.warning("Errore: Valore di ConsumoSuPopolazione negativo. Tupla " + cont + "!");
			}
		}catch(NumberFormatException e){
			risultato = false;
			LOGGER.warning("Errore: Formato di ConsumoSuPopolazione non permesso. Tupla " + cont + "!");
		}
		return risultato;	
	}
	
	public static boolean checkUso(String uso, int cont){
		boolean risultato = true;
		
		if(uso.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di Uso nullo. Tupla " + cont + "!");
		}
		if(!(uso.equals("Residenziale")||uso.equals("Altro"))) {
			risultato = false;
			LOGGER.warning("Errore: Valore di Uso non consentito. Tupla " + cont + "!");
		}
		return risultato;	
	}

	public static boolean checkLatitudine(String latitudine, int cont){
		boolean risultato = true;
		
		if(latitudine.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di Latitudine nullo. Tupla " + cont + "!");
		}
		try{
			String la=latitudine.replace(",",".");
			float lat=Float.parseFloat(la);
			if(lat<16.5||lat>17.5) {
				risultato = false;
				LOGGER.warning("Errore: Valore di Latitudine non compreso tra i valori consentiti. Tupla " + cont + "!");
			}
		}catch(NumberFormatException e){
			risultato = false;
			LOGGER.warning("Errore: Formato di Latitudine non permesso. Tupla " + cont + "!");
		}
		return risultato;	
	}

	public static boolean checkLongitudine(String longitudine, int cont){
		boolean risultato = true;
		
		if(longitudine.isEmpty()){
			risultato = false;
			LOGGER.warning("Errore: Valore di Longitudine nullo. Tupla " + cont + "!");
		}
		try{
			String lo=longitudine.replace(",",".");
			float lon=Float.parseFloat(lo);
			if(lon<41.0||lon>41.5) {
				risultato = false;
				LOGGER.warning("Errore: Valore di Longitudine non compreso tra i valori consentiti. Tupla " + cont + "!");
			}
		}catch(NumberFormatException e){
			risultato = false;
			LOGGER.warning("Errore: Formato di Longitudine non permesso. Tupla " + cont + "!");
		}
		return risultato;	
	}
}