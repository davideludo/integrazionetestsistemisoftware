package procedureETL;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

import procedureETL.FormatoTuplaFinale;

public class CreazioneDataWarehouse {
	
	public ArrayList<FormatoTuplaFinale> dwNewRecords;
	public ArrayList<FormatoTuplaFinale> dwRecords;
	private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private static final String FILE_HEADER = "ID;IDQuartiere;IDMunicipio;IDUso;AnnoMese;ConsumoQuartiere;"
			+ "UtenzeQuartiere;ConsumoCO2;ConsumoMedioUtenza;Quartiere;PopolazioneQuartiere;Anno;"
			+ "NumeroMese;Mese;data;MeseAnno;ConsumoSuPopolazione;Uso;"
			+ "Latitudine;Longitudine;Stagione";

	public CreazioneDataWarehouse(ArrayList<FormatoTuplaFinale> dwnewrecords, ArrayList<FormatoTuplaFinale> dwrecords) {
		this.dwNewRecords = new ArrayList<FormatoTuplaFinale>(dwnewrecords);
		this.dwRecords = new ArrayList<FormatoTuplaFinale>(dwrecords);
	}
	
	private static final String COMMA_DELIMITER = ";";
	private static final String NEW_LINE_SEPARATOR = "\n";
	
	public void writeCsvFile(String fileName) {
		LOGGER.info("Informazioni scrittura tuple nel file finale:");
		FileWriter fileWriter = null;
		int cont = 0;
		
		try {
			fileWriter = new FileWriter(fileName);
			fileWriter.append(FILE_HEADER.toString());
			fileWriter.append(NEW_LINE_SEPARATOR);
			
			// LISTA VECCHI RECORD
			for (FormatoTuplaFinale records : dwRecords) {
				fileWriter.append(String.valueOf(records.getID()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getIDQuartiere()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getIDMunicipio()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getIDUso()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(records.getAnnoMese());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getConsumoQuartiere()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getUtenzeQuartiere()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getConsumoCO2()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getConsumoMedioUtenza()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(records.getQuartiere());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getPopolazioneQuartiere()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getAnno()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getNumeroMese()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(records.getMese());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(records.getData().toString());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(records.getMeseAnno());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getConsumoSuPopolazione()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(records.getUso());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(records.getLatitudine());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(records.getLongitudine());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(records.getStagione());
				fileWriter.append(NEW_LINE_SEPARATOR);
			}
			
			// LISTA NUOVI RECORD
			for (FormatoTuplaFinale records : dwNewRecords) {
				fileWriter.append(String.valueOf(records.getID()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getIDQuartiere()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getIDMunicipio()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getIDUso()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(records.getAnnoMese());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getConsumoQuartiere()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getUtenzeQuartiere()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getConsumoCO2()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getConsumoMedioUtenza()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(records.getQuartiere());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getPopolazioneQuartiere()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getAnno()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(String.valueOf(records.getNumeroMese()));
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(records.getMese());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(records.getData().toString());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(records.getMeseAnno());
				fileWriter.append(COMMA_DELIMITER);
				String consumoSuPop=String.valueOf(records.getConsumoSuPopolazione()).replace(".",",");
				fileWriter.append(consumoSuPop);
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(records.getUso());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(records.getLatitudine());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(records.getLongitudine());
				fileWriter.append(COMMA_DELIMITER);
				fileWriter.append(records.getStagione());
				fileWriter.append(NEW_LINE_SEPARATOR);
			}
			
			LOGGER.info("File CSV creato con successo!");
			LOGGER.info("Nuove Tuple scritte nel file: " + cont);
		}catch (Exception e) {
			LOGGER.warning("Errore nella procedura di scrittura.");
			e.printStackTrace();
		} finally {
			try {
				fileWriter.flush();
				fileWriter.close();
			} catch (IOException e) {
				LOGGER.warning("Errore chiusura file.");
				e.printStackTrace();
			}
		}
	}
}