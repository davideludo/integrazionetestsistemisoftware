package procedureETL;

import java.io.IOException;
import java.util.ArrayList;

import controls.ControlloDati;
import controls.ControlloDuplicati;
import procedureETL.CreazioneDataWarehouse;
import procedureETL.ExtractDataWarehouse;
import procedureETL.TupleDataWarehouse;
import procedureETL.FormatoTuplaFinale;
import procedureETL.FormatoTuplaIniziale;
import exception.LunghezzaIntestazioneException;
import exception.OrdineIntestazioneException;
import procedureETL.ExtractFromCsv;
import logger.MyLogger;

public class StartETL {
	
	public static void main(String[] args) throws LunghezzaIntestazioneException, OrdineIntestazioneException, IOException {
		
		String dataWarehouseFile = "DW Enel.csv"; //FILE PER IL DATAWAREHOUSE
		ExtractFromCsv extractor = new ExtractFromCsv();
		
		ArrayList<FormatoTuplaIniziale> estratti = new ArrayList<FormatoTuplaIniziale>();
		ArrayList<FormatoTuplaIniziale> nonduplicati = new ArrayList<FormatoTuplaIniziale>();
		ArrayList<FormatoTuplaFinale> controllati = new ArrayList<FormatoTuplaFinale>();
		ArrayList<FormatoTuplaFinale> dwrecords = new ArrayList<FormatoTuplaFinale>();
		ArrayList<FormatoTuplaFinale> dascrivere = new ArrayList<FormatoTuplaFinale>();
		
		try {
			MyLogger.setup();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("ERRORE nella creazione del file di log.");
		}
		
		System.out.println("Avvio procedura ETL...");
		System.out.println("Estrazione dati dal file in input...");
		extractor.startExtraction();
		estratti = extractor.getTuple();
		
		System.out.println("Estrazione dati completata!\nAvvio controllo di dati duplicati...");
		ControlloDuplicati controllerduplicati = new ControlloDuplicati(estratti);
		controllerduplicati.generaMapping();
		nonduplicati = controllerduplicati.getNonDuplicati();
		
		System.out.println("Controllo duplicati completato.\nAvvio controllo dati estratti...");

		ControlloDati controller = new ControlloDati(nonduplicati);
		controller.datiFinali();
		controllati = controller.getTupleTrasformate();
		
		System.out.println("Controllo dati completato.\nAvvio creazione dati finali...");

		ExtractDataWarehouse dataWareHouseExtractor = new ExtractDataWarehouse();
		dataWareHouseExtractor.extractionDW();
		dwrecords = dataWareHouseExtractor.getDataWareHouseRecords();

		TupleDataWarehouse dwgenerator = new TupleDataWarehouse(dwrecords, controllati);
		dwgenerator.generateDWNewRecords();
		dascrivere = dwgenerator.getDWNewRecords();
		
		System.out.println("Creazione dati completata.\nAvvio scrittura file...");

		CreazioneDataWarehouse writer = new CreazioneDataWarehouse(dascrivere, dwrecords);
		writer.writeCsvFile(dataWarehouseFile);
		
		System.out.println("Procedura ETL terminata.\nFile CSV pronto per l'utilizzo nel DataWarehouse!\nConsultare il file di log per informazioni e/o errori.");
	}
}