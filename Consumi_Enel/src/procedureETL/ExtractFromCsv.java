package procedureETL;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import procedureETL.ControlloCampo;
import exception.LunghezzaIntestazioneException;
import exception.OrdineIntestazioneException;
import procedureETL.FormatoTuplaIniziale;

public class ExtractFromCsv {
	
	private String csvFile = "src/Database Enel.csv"; //FILE DI INPUT
	private static BufferedReader buffer = null;	
	private String tuplaFile = "";
	private String csvSplit = ";";
	private String dateSplit = "/";
	ArrayList<FormatoTuplaIniziale> tuple = new ArrayList<FormatoTuplaIniziale>();

    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    
    public void startExtraction() throws IOException, LunghezzaIntestazioneException, OrdineIntestazioneException {
    	
		FormatoTuplaIniziale tuplaGenericaIniziale;

    	LOGGER.setLevel(Level.ALL);
    	
    	try {
    		buffer = new BufferedReader(new FileReader(csvFile));
    		
        	String intestazioneFile = buffer.readLine(); 
		    ControlloCampo.checkIntestazione(intestazioneFile);
		    
		    int counterTupleEstratte = 0;
		    
			while((tuplaFile = buffer.readLine()) != null) {
				counterTupleEstratte++;
				tuplaGenericaIniziale = new FormatoTuplaIniziale();
				String[] campo = new String[24];
				campo = tuplaFile.split(csvSplit);
								
				if(ControlloCampo.checkTimedOut(campo[0], counterTupleEstratte)) {
					tuplaGenericaIniziale.setTimed_out(campo[0]);
				}
				if(ControlloCampo.checkTotale(campo[1], counterTupleEstratte)) {
					tuplaGenericaIniziale.setTotale(Integer.parseInt(campo[1]));
				}
				if(ControlloCampo.checkIndice(campo[2], counterTupleEstratte)) {
					tuplaGenericaIniziale.setIndice(campo[2]);
				}
				if(ControlloCampo.checkType(campo[3], counterTupleEstratte)) {
					tuplaGenericaIniziale.setType(campo[3]);
				}
				if(ControlloCampo.checkID(campo[4], counterTupleEstratte)) {
					tuplaGenericaIniziale.setID(Integer.parseInt(campo[4]));
				}
				if(ControlloCampo.checkIDQuartiere(campo[5], counterTupleEstratte)) {
					tuplaGenericaIniziale.setIDQuartiere(Integer.parseInt(campo[5]));
				}
				if(ControlloCampo.checkIDMunicipio(campo[6], counterTupleEstratte)) {
					tuplaGenericaIniziale.setIDMunicipio(Integer.parseInt(campo[6]));
				}
				if(ControlloCampo.checkIDUso(campo[7], counterTupleEstratte)) {
					tuplaGenericaIniziale.setIDUso(Integer.parseInt(campo[7]));
				}
				if(ControlloCampo.checkAnnoMese(campo[8], counterTupleEstratte)) {
					tuplaGenericaIniziale.setAnnoMese(campo[8]);
				}
				if(ControlloCampo.checkConsumoQuartiere(campo[9], counterTupleEstratte)) {
					tuplaGenericaIniziale.setConsumoQuartiere(Long.parseLong(campo[9]));
				}
				if(ControlloCampo.checkUtenzeQuartiere(campo[10], counterTupleEstratte)) {
					tuplaGenericaIniziale.setUtenzeQuartiere(Integer.parseInt(campo[10]));
				}
				if(ControlloCampo.checkCO2(campo[11], counterTupleEstratte)) {
					tuplaGenericaIniziale.setConsumoCO2(Long.parseLong(campo[11]));
				}
				if(ControlloCampo.checkConsumoMedio(campo[12], counterTupleEstratte)) {
					tuplaGenericaIniziale.setConsumoMedioUtenza(Integer.parseInt(campo[12]));
				}
				if(ControlloCampo.checkQuartiere(campo[13], counterTupleEstratte)) {
					tuplaGenericaIniziale.setQuartiere(campo[13]);
				}
				if(ControlloCampo.checkPopolazioneQuartiere(campo[14], counterTupleEstratte)) {
					tuplaGenericaIniziale.setPopolazioneQuartiere(Integer.parseInt(campo[14]));
				}
				if(ControlloCampo.checkAnno(campo[15], counterTupleEstratte)) {
					tuplaGenericaIniziale.setAnno(Integer.parseInt(campo[15]));
				}
				if(ControlloCampo.checkNumeroMese(campo[16], counterTupleEstratte)) {
					tuplaGenericaIniziale.setNumeroMese(Integer.parseInt(campo[16]));
				}
				if(ControlloCampo.checkMese(campo[17], counterTupleEstratte)) {
					tuplaGenericaIniziale.setMese(campo[17]);
				}
				String[] dataCsv = campo[18].split(dateSplit);
				if (ControlloCampo.checkData(campo[18], counterTupleEstratte)) {
					tuplaGenericaIniziale.data.setDay(Integer.parseInt(dataCsv[0]));
					tuplaGenericaIniziale.data.setMounth(Integer.parseInt(dataCsv[1]));
					tuplaGenericaIniziale.data.setYear(Integer.parseInt(dataCsv[2]));
				} else {
					tuplaGenericaIniziale.data.setDay(1);
					tuplaGenericaIniziale.data.setMounth(1);
					tuplaGenericaIniziale.data.setYear(1990);
				}
				if(ControlloCampo.checkMeseAnno(campo[19], counterTupleEstratte)) {
					tuplaGenericaIniziale.setMeseAnno(campo[19]);
				}
				if(ControlloCampo.checkConsumoSuPopolazione(campo[20], counterTupleEstratte)) {
					tuplaGenericaIniziale.setConsumoSuPopolazione(Float.parseFloat(campo[20].replace(",", ".")));
				}
				
				if(ControlloCampo.checkUso(campo[21], counterTupleEstratte)) {
					tuplaGenericaIniziale.setUso(campo[21]);
				}
				if(ControlloCampo.checkLatitudine(campo[22], counterTupleEstratte)) {
					tuplaGenericaIniziale.setLatitudine(campo[22]);
				}
				if(ControlloCampo.checkLongitudine(campo[23], counterTupleEstratte)) {
					tuplaGenericaIniziale.setLongitudine(campo[23]);
				}
				tuple.add(tuplaGenericaIniziale);	
			}
			LOGGER.info("Tuple estratte file di input: " + counterTupleEstratte);
        	}catch (FileNotFoundException e) {
           	 e.printStackTrace();
            } catch (IOException er) { 
                er.printStackTrace();
            } finally {
                if (buffer != null) {
                    try {
                    	buffer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
    }
    public ArrayList<FormatoTuplaIniziale> getTuple(){
    	return this.tuple;
    }
}