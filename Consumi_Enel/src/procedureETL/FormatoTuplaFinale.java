package procedureETL;

public class FormatoTuplaFinale {
	
	private int ID;
	private int IDQuartiere;
	private int IDMunicipio;
	private int IDUso;
	private String AnnoMese;
	private long ConsumoQuartiere;
	private int UtenzeQuartiere;
	private long ConsumoCO2;
	private int ConsumoMedioUtenza;
	private String Quartiere;
	private int PopolazioneQuartiere;
	private int Anno;
	private int NumeroMese;
	private String Mese;
	Data data;
	private String MeseAnno;
	private float ConsumoSuPopolazione;
	private String Uso;
	private String Latitudine;
	private String Longitudine;
	private String Stagione;

	public FormatoTuplaFinale() {
		data=new Data();
	}
		
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public int getIDQuartiere() {
		return IDQuartiere;
	}
	public void setIDQuartiere(int iDQuartiere) {
		IDQuartiere = iDQuartiere;
	}
	public int getIDMunicipio() {
		return IDMunicipio;
	}
	public void setIDMunicipio(int iDMunicipio) {
		IDMunicipio = iDMunicipio;
	}
	public int getIDUso() {
		return IDUso;
	}
	public void setIDUso(int iDUso) {
		IDUso = iDUso;
	}
	public String getAnnoMese() {
		return AnnoMese;
	}
	public void setAnnoMese(String annoMese) {
		AnnoMese = annoMese;
	}
	public long getConsumoQuartiere() {
		return ConsumoQuartiere;
	}
	public void setConsumoQuartiere(long consumoQuartiere) {
		ConsumoQuartiere = consumoQuartiere;
	}
	public int getUtenzeQuartiere() {
		return UtenzeQuartiere;
	}
	public void setUtenzeQuartiere(int utenzeQuartiere) {
		UtenzeQuartiere = utenzeQuartiere;
	}
	public long getConsumoCO2() {
		return ConsumoCO2;
	}
	public void setConsumoCO2(long consumoCO2) {
		ConsumoCO2 = consumoCO2;
	}
	public int getConsumoMedioUtenza() {
		return ConsumoMedioUtenza;
	}
	public void setConsumoMedioUtenza(int consumoMedioUtenza) {
		ConsumoMedioUtenza = consumoMedioUtenza;
	}
	public String getQuartiere() {
		return Quartiere;
	}
	public void setQuartiere(String quartiere) {
		Quartiere = quartiere;
	}
	public int getPopolazioneQuartiere() {
		return PopolazioneQuartiere;
	}
	public void setPopolazioneQuartiere(int popolazioneQuartiere) {
		PopolazioneQuartiere = popolazioneQuartiere;
	}
	public int getAnno() {
		return Anno;
	}
	public void setAnno(int anno) {
		Anno = anno;
	}
	public int getNumeroMese() {
		return NumeroMese;
	}
	public void setNumeroMese(int numeroMese) {
		NumeroMese = numeroMese;
	}
	public String getMese() {
		return Mese;
	}
	public void setMese(String mese) {
		Mese = mese;
	}
	public Data getData() {
		return data;
	}
	public void setData(Data data) {
		this.data = data;
	}
	public String getMeseAnno() {
		return MeseAnno;
	}
	public void setMeseAnno(String meseAnno) {
		MeseAnno = meseAnno;
	}
	public float getConsumoSuPopolazione() {
		return ConsumoSuPopolazione;
	}
	public void setConsumoSuPopolazione(float consumoSuPopolazione) {
		ConsumoSuPopolazione = consumoSuPopolazione;
	}
	public String getUso() {
		return Uso;
	}
	public void setUso(String uso) {
		Uso = uso;
	}
	public String getLatitudine() {
		return Latitudine;
	}
	public void setLatitudine(String latitudine) {
		Latitudine = latitudine;
	}
	public String getLongitudine() {
		return Longitudine;
	}
	public void setLongitudine(String longitudine) {
		Longitudine = longitudine;
	}
	public String getStagione() {
		return Stagione;
	}
	public void setStagione(String stagione) {
		Stagione = stagione;
	}
	
	
	

}
