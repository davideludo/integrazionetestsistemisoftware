package procedureETL;

public class FormatoTuplaIniziale {
	
	private String timed_out;
	private int Totale;
	private String Indice;
	private String Type;
	private int ID;
	private int IDQuartiere;
	private int IDMunicipio;
	private int IDUso;
	private String AnnoMese;
	private long ConsumoQuartiere;
	private int UtenzeQuartiere;
	private long ConsumoCO2;
	private int ConsumoMedioUtenza;
	private String Quartiere;
	private int PopolazioneQuartiere;
	private int Anno;
	private int NumeroMese;
	private String Mese;
	Data data;
	private String MeseAnno;
	private float ConsumoSuPopolazione;
	private String Uso;
	private String Latitudine;
	private String Longitudine;
	
	public FormatoTuplaIniziale() {
		data=new Data();
	}
	
	public String getTimed_out() {
		return timed_out;
	}
	
	public void setTimed_out(String timed_out) {
		this.timed_out = timed_out;
	}
	
	public int getTotale() {
		return Totale;
	}
	
	public void setTotale(int Totale) {
		this.Totale = Totale;
	}
	
	public String getIndice() {
		return Indice;
	}
	
	public void setIndice(String Indice) {
		this.Indice = Indice;
	}
	
	public String getType() {
		return Type;
	}
	
	public void setType(String Type) {
		this.Type = Type;
	}
	
	public int getID() {
		return ID;
	}
	
	public void setID(int ID) {
		this.ID = ID;
	}
	
	public int getIDQuartiere() {
		return IDQuartiere;
	}
	
	public void setIDQuartiere(int IDQuartiere) {
		this.IDQuartiere = IDQuartiere;
	}
	
	public int getIDMunicipio() {
		return IDMunicipio;
	}
	
	public void setIDMunicipio(int IDMunicipio) {
		this.IDMunicipio = IDMunicipio;
	}
	
	public int getIDUso() {
		return IDUso;
	}
	
	public void setIDUso(int IDUso) {
		this.IDUso = IDUso;
	}
	
	public String getAnnoMese() {
		return AnnoMese;
	}
	
	public void setAnnoMese(String AnnoMese) {
		this.AnnoMese = AnnoMese;
	}
	
	public long getConsumoQuartiere() {
		return ConsumoQuartiere;
	}
	
	public void setConsumoQuartiere(long ConsumoQuartiere) {
		this.ConsumoQuartiere = ConsumoQuartiere;
	}
	
	public int getUtenzeQuartiere() {
		return UtenzeQuartiere;
	}
	
	public void setUtenzeQuartiere(int UtenzeQuartiere) {
		this.UtenzeQuartiere = UtenzeQuartiere;
	}
	
	public long getConsumoCO2() {
		return ConsumoCO2;
	}
	
	public void setConsumoCO2(long ConsumoCO2) {
		this.ConsumoCO2 = ConsumoCO2;
	}
	
	public int getConsumoMedioUtenza() {
		return ConsumoMedioUtenza;
	}
	
	public void setConsumoMedioUtenza(int ConsumoMedioUtenza) {
		this.ConsumoMedioUtenza = ConsumoMedioUtenza;
	}
	
	public String getQuartiere() {
		return Quartiere;
	}
	
	public void setQuartiere(String Quartiere) {
		this.Quartiere = Quartiere;
	}
	
	public int getPopolazioneQuartiere() {
		return PopolazioneQuartiere;
	}
	
	public void setPopolazioneQuartiere(int PopolazioneQuartiere) {
		this.PopolazioneQuartiere = PopolazioneQuartiere;
	}
	
	public int getAnno() {
		return Anno;
	}
	
	public void setAnno(int Anno) {
		this.Anno = Anno;
	}
	
	public int getNumeroMese() {
		return NumeroMese;
	}
	
	public void setNumeroMese(int NumeroMese) {
		this.NumeroMese = NumeroMese;
	}
	
	public String getMese() {
		return Mese;
	}
	
	public void setMese(String Mese) {
		this.Mese = Mese;
	}
	
	public Data getData() {
		return data;
	}
	
	public void setData(Data data) {
		this.data = data;
	}
	
	public String getMeseAnno() {
		return MeseAnno;
	}
	
	public void setMeseAnno(String MeseAnno) {
		this.MeseAnno = MeseAnno;
	}
	
	public float getConsumoSuPopolazione() {
		return ConsumoSuPopolazione;
	}
	
	public void setConsumoSuPopolazione(float ConsumoSuPopolazione) {
		this.ConsumoSuPopolazione = ConsumoSuPopolazione;
	}
	
	public String getUso() {
		return Uso;
	}
	
	public void setUso(String Uso) {
		this.Uso = Uso;
	}
	
	public String getLatitudine() {
		return Latitudine;
	}
	
	public void setLatitudine(String Latitudine) {
		this.Latitudine = Latitudine;
	}
	
	public String getLongitudine() {
		return Longitudine;
	}
	
	public void setLongitudine(String Longitudine) {
		this.Longitudine = Longitudine;
	}
	
}
