package procedureETL;

import procedureETL.Data;

public class Data {
	
	private int giorno;
	private int mese;
	private int anno;
	
	public int getDay() {
		return giorno;
	}
	
	public void setDay(int day) {
		this.giorno = day;
	}
	
	public int getMounth() {
		return mese;
	}
	
	public void setMounth(int mounth) {
		this.mese = mounth;
	}
	
	public int getYear() {
		return anno;
	}
	
	public void setYear(int anno) {
		this.anno = anno;
	}
	
	public String calcoloStagione() {
			
			String stagione;
			
			if((giorno>=23 && mese==12) || (giorno<=20 && mese<=3)) {
				
				stagione = "Inverno"; 
			
			}else if(((giorno>=21 && mese==3)) || (mese>3 && mese<6) || (giorno <=20 && mese==6)) {
				
				stagione = "Primavera";
			
			}else if((giorno>=21 && mese==6) || (mese >6 && mese <9) || (giorno<=22 && mese==9)) {
				
				stagione = "Estate";
			
			}else {
				stagione = "Autunno";
			}
			
			return stagione;
		
	}
	
	@Override
	public String toString() {
		String risultato;
		risultato = this.getDay() + "-" + this.getMounth() + "-" +this.getYear();
		return risultato;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + anno;
		result = prime * result + giorno;
		result = prime * result + mese;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Data other = (Data) obj;
		if (anno != other.anno)
			return false;
		if (giorno != other.giorno)
			return false;
		if (mese != other.mese)
			return false;
		return true;
	}

}
