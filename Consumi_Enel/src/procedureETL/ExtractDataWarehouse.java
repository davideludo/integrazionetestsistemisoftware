package procedureETL;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

import exception.LunghezzaIntestazioneException;
import exception.OrdineIntestazioneException;
import procedureETL.FormatoTuplaFinale;

public class ExtractDataWarehouse {
	
	private final String[] intestazioneStandard = { "ID","IDQuartiere","IDMunicipio","IDUso","AnnoMese","ConsumoQuartiere",
		"UtenzeQuartiere","ConsumoCO2","ConsumoMedioUtenza","Quartiere","PopolazioneQuartiere","Anno",	
		"NumeroMese","Mese","data","MeseAnno","ConsumoSuPopolazione","Uso","Latitudine","Longitudine","Stagione"};

	private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private ArrayList<FormatoTuplaFinale> recordsDataWareHouse = new ArrayList<FormatoTuplaFinale>();
	private String dwFile = "DW Enel.csv";
	private BufferedReader buffer = null;
	private String line = "";
	private String cvsSplitBy = ";";
	private String dateSplitBy = "-";
	private static String[] campiIntestazioneFileDW = new String[21];
	
	public void extractionDW() throws LunghezzaIntestazioneException, OrdineIntestazioneException {
		
		try {
			
			FormatoTuplaFinale tuplaFinale;
			buffer = new BufferedReader(new FileReader(dwFile));
			
			String intestazioneFileDW = buffer.readLine();	
			campiIntestazioneFileDW = intestazioneFileDW.split(cvsSplitBy);
			
			// CONTROLLI INTESTAZIONE
			if (campiIntestazioneFileDW.length != intestazioneStandard.length)throw new LunghezzaIntestazioneException();

			for (int i = 0; i < intestazioneStandard.length; i++) {
				if (campiIntestazioneFileDW[i].compareTo(intestazioneStandard[i]) != 0)
					throw new OrdineIntestazioneException();
			}
			
			// POPOLAMENTO DATAWAREHOUSE
			int counterTuple = 0;
			while ((line = buffer.readLine()) != null) {
				counterTuple++;
				tuplaFinale = new FormatoTuplaFinale();
				String[] campo;
				campo = line.split(cvsSplitBy);				
				tuplaFinale.setID(Integer.parseInt(campo[0]));
				tuplaFinale.setIDQuartiere(Integer.parseInt(campo[1]));
				tuplaFinale.setIDMunicipio(Integer.parseInt(campo[2]));
				tuplaFinale.setIDUso(Integer.parseInt(campo[3]));
				tuplaFinale.setAnnoMese(campo[4]);
				tuplaFinale.setConsumoQuartiere(Long.parseLong(campo[5]));
				tuplaFinale.setUtenzeQuartiere(Integer.parseInt(campo[6]));
				tuplaFinale.setConsumoCO2(Long.parseLong(campo[7]));
				tuplaFinale.setConsumoMedioUtenza(Integer.parseInt(campo[8]));
				tuplaFinale.setQuartiere(campo[9]);
				tuplaFinale.setPopolazioneQuartiere(Integer.parseInt(campo[10]));
				tuplaFinale.setAnno(Integer.parseInt(campo[11]));
				tuplaFinale.setNumeroMese(Integer.parseInt(campo[12]));
				tuplaFinale.setMese(campo[13]);
				String[] data = campo[14].split(dateSplitBy);
				tuplaFinale.data.setDay(Integer.parseInt(data[0]));
				tuplaFinale.data.setMounth(Integer.parseInt(data[1]));
				tuplaFinale.data.setYear(Integer.parseInt(data[2]));
				tuplaFinale.setMeseAnno(campo[15]);
				String fl = campo[16].replace(",", ".");
				tuplaFinale.setConsumoSuPopolazione(Float.parseFloat(fl));
				tuplaFinale.setUso(campo[17]);
				tuplaFinale.setLatitudine(campo[18]);
				tuplaFinale.setLongitudine(campo[19]);
				tuplaFinale.setStagione(campo[20]);
				recordsDataWareHouse.add(tuplaFinale);
			}
			
			LOGGER.info("Tuple estratte dal DataWarehouse: " + counterTuple);
			
		}catch (FileNotFoundException e) {
			System.out.println("Nessun file DataWarehouse precedente trovato. Creato nuovo file.");
		} catch (IOException er) {
			er.printStackTrace();
		} finally {
			if (buffer != null) {
				try {
					buffer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public ArrayList<FormatoTuplaFinale> getDataWareHouseRecords() {
		return recordsDataWareHouse;
	}
}
